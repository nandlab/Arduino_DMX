/* ESP 32 mit der Eigenschaft Taster abzufragen, Relais zu schalten und UDP Nachrichten zu verarbeiten.
 *
 *  29.04.2019 Entwicklung einer Programmierung, die es erlaubt Taster abzufragen und Relais zu schalten.
 *             WLAN Konnektivität eingebaut
 *             UDP Nachrichten eingebaut
 *             Empfang UDP einprogrammiert
 *             Bitte nicht vergessen die OPEN und CLOSE Befehle anzupassen
 *             Code wurde für DMX angepasst. Verschickt jetzt DMX und Qlab commands
 *  12.02.2020 Programmcode für High Voltage in Bochum angepasst
 *             DMX Kanäle einprogrammiert
 *             UDPs an Betreuungsstation und an Qlab einprogrammiert
 *             Empfang der UDPs von den ESPs zur Tasterabfrage
 *  26.02.2020 Zugangsdaten für den Router angepasst
 *             Alarm kommt mit Hauptbereich
 *             Rätsel 5 ist ab jetzt unbenutzt
 *  04.03.2020 Alarmspiel einprogrammiert
 *
 *  2021-03-17 Heartbeat Stop
 *  2021-08-25 Web server commented out, because it did nothing
 *
 */

int Anzahl=8;

#define DMX_SERIAL_OUTPUT_PIN 18
#define DMX_DIRECTION_PIN 21

#include <WiFi.h>
#include <WiFiUdp.h>

#include <TOD.h>                    //DMX Bibliotheken
#include <LXESP32DMX.h>
#include <LXHardwareSerial.h>
#include <rdm_utility.h>
#include <UID.h>

#include "config.h"
#include "light.h"

typedef void (*riddle_handler_t)(void);

// WiFi. Zuerst wird ein WLAN-Client probiert, falls das fehlschlägt wird ein Accesspoint
byte my_WiFi_Mode = 0;

#define UDP_TX_PACKET_MAX_SIZE 64
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];

unsigned long HAZER_time;
unsigned long HAZER_starttime;
unsigned int HAZER_wert = 0;

WiFiUDP udp;

char mystring[32];
char buf[20];

void setLight(const uint8_t lightSituation[]) {
  for (int i=0; i<NUM_DMX_SLOTS; i++) {
    ESP32DMX.setSlot(i+1, lightSituation[i]);
  }
}

//--------------------------Auf UDP Nachrichten warten----------------------------
void checkUDP()
{
  /* if there's data available, read a packet */
  int packetSize = udp.parsePacket();
  if(packetSize)
  {
    /* read the packet into packetBufffer */
    udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE-1);

    /* for debug only
     * write packetBuffer to serial */
    Serial.print("Nachricht: ");
    Serial.println(packetBuffer);

    /* send "ACK" as reply
    udp.beginPacket(udp.remoteIP(), udp.remotePort());
    udp.write('ACK');
    udp.endPacket(); */
  }
  delay(10);
}

//--------------------------UDP Nachrichten an Qlab schicken-----------------------
void sendQlab(String text)
{
  udp.beginPacket(recipientIP, qlabPort);
  udp.print(text);
  udp.endPacket();
  delay(10);
  for (int i=0; i<UDP_TX_PACKET_MAX_SIZE; i++) {
    packetBuffer[i] = '\0';
  }
  delay(20);
}

//--------------------------UDP Nachrichten schicken--------------------------------------
void sendSupport(String text)
{
  udp.beginPacket(recipientIP, supportPort);
  udp.print(text);
  udp.endPacket();
  delay(10);
  for (int i=0; i<UDP_TX_PACKET_MAX_SIZE; i++) {
    packetBuffer[i] = '\0';
  }
  delay(20);
}

//--------------------------UDP Nachrichten an ESP schicken--------------------------------------
void sendESP(String text)
{
  udp.beginPacket(recipientIP, espPort);
  udp.print(text);
  udp.endPacket();
  delay(10);
  for (int i=0; i<UDP_TX_PACKET_MAX_SIZE; i++) {
    packetBuffer[i] = '\0';
  }
  delay(20);
}

//--------------------------------------Sound und UDP Steuerung --------------------
void soundUDP()
{
  sprintf(buf, "/cue/200/stop");
  sendQlab(buf);
  delay(10);

  sprintf(buf,"R2 Spielstart");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/200/start");
  sendQlab(buf);
  delay(30);
}

void riddle1()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"Zelle 1 auf");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/411/start");
  sendQlab(buf);
  setLight(resetlightOffArray);

  delay(2000);

  setLight(riddle1Array);
  delay(30);
}

void riddle2()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"Zelle 2 auf");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  setLight(resetlightOffArray);
  sprintf(buf, "/cue/412/start");
  sendQlab(buf);
  delay(1000);

  setLight(riddle2Array);
  delay(30);
}

void riddle3()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"Hundezwinger");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);
  setLight(resetlightOffArray);
  sprintf(buf, "/cue/413/start");
  sendQlab(buf);
  delay(3000);
  setLight(riddle3_1Array);
  delay(8000);
  setLight(riddle3_2Array);
  delay(30);
}

void riddle4()
{
  delay(100);

  sprintf(buf,"Alarm. Hauptbereich auf");
  Serial.println(buf);
  sendSupport(buf);

  delay(30);

  sprintf(buf, "/cue/414/start");
  sendQlab(buf);
  delay(30);

  delay(100);

  sprintf(buf, "/cue/415/start");
  sendQlab(buf);
  delay(30);

  setLight(resetlightOffArray);

  delay(30);

  setLight(riddle4Array);
  delay(30);
}

void riddle5() {
  // Not used
}

void riddle6()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"4 Raedchen");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/416/start");
  sendQlab(buf);

  setLight(resetlightOffArray);

  delay(1000);

  setLight(riddle6Array);
  delay(30);
}

void riddle7()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"4 Taster");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/417/start");
  sendQlab(buf);
  delay(30);

  setLight(riddle7Array);
  delay(30);
}

void riddle8()
{
  sprintf(buf, "/cue/415/stop");
  sendQlab(buf);

  delay(10);

  sprintf(buf, "/cue/413/stop");
  sendQlab(buf);

  delay(10);

  sprintf(buf,"Alarm aus");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/418/start");
  sendQlab(buf);

  setLight(resetlightOffArray);

  delay(500);

  setLight(riddle8Array);
  delay(30);
}

const riddle_handler_t riddleHandlers[8] = {
  riddle1,
  riddle2,
  riddle3,
  riddle4,
  riddle5,
  riddle6,
  riddle7,
  riddle8
};

void setup() {
  Serial.begin(115200);
  Serial.println("Sketch: Arduino_DMX");
  Serial.println("LOCKED DMX Steuerung und Qlab Commands.");
  Serial.println();
  Serial.print("Dieser ESP32 hat die Nummer: ");
  Serial.println(esp32);
  Serial.println();
  Serial.print("Empfänger-IP: ");
  Serial.println(recipientIP);
  Serial.print("Empfänger-Port: ");
  Serial.println(localPort);
  Serial.print("Qlab-Port: ");
  Serial.println(qlabPort);
  Serial.println();
  Serial.print("Aktuelle Systemzeit ");
  HAZER_time = millis();
  Serial.println(HAZER_time);

  pinMode(DMX_DIRECTION_PIN, OUTPUT);
  digitalWrite(DMX_DIRECTION_PIN, HIGH);

  pinMode(DMX_SERIAL_OUTPUT_PIN, OUTPUT);
  ESP32DMX.startOutput(DMX_SERIAL_OUTPUT_PIN);

  setLight(resetlightOnArray);

  //esp_task_wdt_feed();
  vTaskDelay(100);
  Serial.println("Resetlight an");
  //delay(30000);
  //DmxSimple.usePin(dmxPin);
  //Serial.println("Relais an GPIO: 19, 18, 5, 17, 16, 4, 0, 2");
  //Serial.println("Taster an GPIO: 32, 33, 25, 26, 27, 14, 21, 13");
  Serial.println();
  delay(100);
  Serial.println("WLAN starten");

  WiFi_Start_STA();
  // if (my_WiFi_Mode == 0) WiFi_Start_AP();

  /* UDP Server das 1. Mal starten */
  Serial.print("UDP Server start: ");
  Serial.println (udp.begin(localPort) ? "success" : "failed");

  /* UDP Client: send UDP Ready  3x, falls es nicht klappt*/
  for(int i=0; i<1; i++){
    Serial.print("\n  ESP: ");
    sprintf(mystring, "%03i UDP READY", esp32);
    Serial.println(mystring);
    udp.beginPacket(supportPort, supportPort);
    udp.print(mystring);
    udp.endPacket();
    //sendSupport(mystring);
    delay(100);
  }
}

void loop() {

  checkUDP();

  /*
    HAZER_starttime = millis();
    if(HAZER_wert==0){
      if(HAZER_starttime - HAZER_time > 10000){
        Serial.println("HAZER auf: 150");
        Serial.print("HAZER_time auf: ");
        Serial.println(HAZER_time);
        Serial.print("HAZER_starttime auf: ");
        Serial.println(HAZER_starttime);
        HAZER_wert=150;
        HAZER_time=millis();
        ESP32DMX.setSlot(9 , HAZER_wert);
      }
    }
    else if(HAZER_wert == 150){
      if(HAZER_starttime - HAZER_time > 10000){
        HAZER_wert=0;
        Serial.println("HAZER wieder auf 0");
        HAZER_time=millis();
        ESP32DMX.setSlot(9 , HAZER_wert);
      }
    }
  */

  //--------------------------UDP "Reset an ESPs"--------------------------------------
  /* strncmp für n Zeichen wird 0 falls gleich, daher Abfrage auf Ungleich (!) */
  if (!strncmp(packetBuffer, "ResetAll", 8))
  {
    for(int i=0; i<1; i++)
    {
      sprintf(buf, "/cue/415/stop");
      sendQlab(buf);

      sprintf(buf,"Reset");
      Serial.println(buf);
      sendESP(buf);
    }
    delay(30);
  }

  //--------------------------UDP "Resetlight an"--------------------------------------
  /* strncmp für n Zeichen wird 0 falls gleich, daher Abfrage auf Ungleich (!) */
  if (!strncmp(packetBuffer, "Resetlight", 10))
  {
    for(int i=0; i<1; i++)
    {
      setLight(resetlightOnArray);
      sprintf(buf,"R4 Resetlight an");
      Serial.println(buf);
      sendSupport(buf);
      delay(30);
    }
  }

  //--------------------------UDP "Resetlight aus"--------------------------------------
  if (!strncmp(packetBuffer, "Nolight", 7))
  {
    for(int i=0; i<1; i++)
    {
      setLight(resetlightOffArray);
      sprintf(buf,"R4 Resetlight aus");
      Serial.println(buf);
      sendSupport(buf);
      delay(30);
    }
  }

  //--------------------------UDP "Hinweis"--------------------------------------
  if (!strncmp(packetBuffer, "R4Hinweissound", 14))
  {
    sprintf(buf,"/cue/490/start");
    Serial.println(buf);
    sendQlab(buf);
    delay(30);
  }

  //--------------------------UDP "Einlass"--------------------------------------
  if (!strncmp(packetBuffer, "310 Einlass", 11))
  {
    for(int i=0; i<1; i++)                         //Schleife eigentlich unnötig, aber vielleicht irgendwann hilfreich
    {
      setLight(resetlightOffArray);
      sprintf(buf,"R3 Einlasslicht an");
      Serial.println(buf);
      sendSupport(buf);
      delay(30);
    }
  }

  //--------------------------UDP "spielstart"--------------------------------------
  if (!strncmp(packetBuffer, "Spielstart", 12))
  {
    spielstart();
    /* sprintf(buf,"R2 Spielstart");
    Serial.println(buf);
    sendQlab(buf); */
  }

  int relay;
  char relay_state[16];
  int s = sscanf(packetBuffer, "400 Relais %d %8s", &relay, relay_state);
  if (s == 2 && !strcmp(relay_state, "Aus") && relay >= 1 && relay <= 8) {
    relay -= 1;
    riddleHandlers[relay]();
  }

  //--------------------------UDP "Spiel Ende"-------------------------------------------
  if (!strncmp(packetBuffer, "HIGH VOLTAGE GAME WON", 21))
  {
    gamewon();
  }

  //--------------------------UDP "Kabel nach und nach"----------------------------------
  if (!strncmp(packetBuffer, "Volume auf 0", 12))
  {
    sprintf(buf, "/cue/427/stop");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/428/stop");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/429/stop");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/428/sliderLevel/0 -10");
    sendQlab(buf);
    delay(10);
  }

  if (!strncmp(packetBuffer, "Volume auf 1", 12))
  {
    sprintf(buf, "/cue/427/start");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/428/sliderLevel/0 -6");
    sendQlab(buf);
    delay(30);
  }

  if (!strncmp(packetBuffer, "Volume auf 2", 12))
  {
    sprintf(buf, "/cue/427/start");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/428/sliderLevel/0 -2");
    sendQlab(buf);
    delay(30);
  }

  if (!strncmp(packetBuffer, "Volume auf 3", 12))
  {
    sprintf(buf, "/cue/427/start");
    sendQlab(buf);
    delay(10);
    sprintf(buf, "/cue/428/sliderLevel/0 2");
    sendQlab(buf);
    delay(30);
  }

  //--------------------------UDP "Kabel 1 dran"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 1 dran", 16))
  {
    ESP32DMX.setSlot(9 , 255);
  }

  //--------------------------UDP "Kabel 2 dran"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 2 dran", 16))
  {
    ESP32DMX.setSlot(12 , 255);
  }

  //--------------------------UDP "Kabel 3 dran"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 3 dran", 16))
  {
    ESP32DMX.setSlot(11 , 255);
  }

  //--------------------------UDP "Kabel 1 ab"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 1 wieder ab", 21))
  {
    ESP32DMX.setSlot(9 , 0);
  }

  //--------------------------UDP "Kabel 2 ab"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 2 wieder ab", 21))
  {
    ESP32DMX.setSlot(12 , 0);
  }

  //--------------------------UDP "Kabel 3 ab"--------------------------------------
  if (!strncmp(packetBuffer, "402 Kabel 3 wieder ab", 21))
  {
    ESP32DMX.setSlot(11 , 0);
  }

  //--------------------------UDP "Alarmrunden"--------------------------------------
  /* UDP (Receive), Close Relais steuert ein Relais auf Ein */
  if (!strncmp(packetBuffer, "402 Runde", 9))
  {
    int iLen, iDigit, i;
    iLen = strlen(packetBuffer);
    for(i=0; i<iLen; i++){
      if(isdigit((int)packetBuffer[i])) {
        iDigit = atoi(packetBuffer+i);
        // Serial.print("Ich bin deine Zahl: ");     /* Die letzte Zahl gewinnt! */
        // Serial.println(iDigit);
        while(isdigit((int)packetBuffer[++i]));
        i++;
      }
    }
    if ( iDigit >= 0 && iDigit <= 9 ) // hier die maximale Spielrundenanzahl von 9 beachten oder ändern
    {
      sprintf(buf,"/cue/430/start");  // evetuell bei idigit noch -1 einsetzen da im Programm ab 0 gezählt wird
      Serial.println(buf);
      sendQlab(buf);
    }
    else
    {
      sprintf(buf,"%03i Relais %01i NOK", esp32, iDigit);
      Serial.println(buf);
      sendQlab(buf);
    }
  }

  //--------------------------UDP "Alarmrunden verkackt"--------------------------------------
  /* UDP (Receive), Close Relais steuert ein Relais auf Ein */
  if (!strncmp(packetBuffer, "402 verkackt in Runde", 12))
  {
    int iLen, iDigit, i;
    iLen = strlen(packetBuffer);
    for(i=0; i<iLen; i++){
      if(isdigit((int)packetBuffer[i])) {
        iDigit = atoi(packetBuffer+i);
        // Serial.print("Ich bin deine Zahl: ");     /* Die letzte Zahl gewinnt! */
        // Serial.println(iDigit);
        while(isdigit((int)packetBuffer[++i]));
      }
    }
    if ( iDigit >= 0 && iDigit <= 9 ) // hier die maximale Spielrundenanzahl von 9 beachten oder ändern
    {
      sprintf(buf,"/cue/431/start");  // evetuell bei idigit noch -1 einsetzen da im Programm ab 0 gezählt wird
      Serial.println(buf);
      sendQlab(buf);
    }
    else
    {
      sprintf(buf,"%03i Relais %01i NOK", esp32, iDigit);
      Serial.println(buf);
      sendQlab(buf);
    }
  }

  //--------------------------UDP "Hinweis"--------------------------------------
  if (!strncmp(packetBuffer, "Alarm ausgeschaltet", 19))
  {
    sprintf(buf,"400 Open Relais 8");
    Serial.println(buf);
    sendESP(buf);
    delay(30);
  }

  for (int i=0; i<UDP_TX_PACKET_MAX_SIZE; i++) {
    packetBuffer[i] = '\0';
  }
  delay(20);
}

//--------------------------WLAN-Programm--------------------------------------
void WiFi_Start_STA() {
  unsigned long timeout;

  WiFi.mode(WIFI_STA);   //  Workstation

  WiFi.begin(ssid, password);
  timeout = millis() + 70000L;
  while (WiFi.status() != WL_CONNECTED && millis() < timeout) {
    delay(10);
  }

  if (WiFi.status() == WL_CONNECTED) {
    // server.begin();
    my_WiFi_Mode = WIFI_STA;

    Serial.print("Verbundene IP-Adresse: ");
    for (int i = 0; i < 3; i++) {
      Serial.print( WiFi.localIP()[i]);
      Serial.print(".");
    }
    Serial.println(WiFi.localIP()[3]);
  }
}

void spielstart()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"R4 Spielstart");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/400/start");
  sendQlab(buf);

  delay(89200);

  setLight(gamestartArray);
  delay(30);
}

void gamewon()
{
  //sprintf(buf, "/cue/200/stop");
  //sendQlab(buf);

  delay(10);

  sprintf(buf,"Spielende");
  Serial.println(buf);
  sendSupport(buf);

  delay(10);

  sprintf(buf, "/cue/400/stop");
  sendQlab(buf);
  delay(30);
  sprintf(buf, "/cue/419/start");
  sendQlab(buf);
  delay(30);
  setLight(gamewon1Array);
  delay(2000);
  setLight(gamewon2Array);
  delay(30);
}
