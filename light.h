#ifndef LIGHT_H_
#define LIGHT_H_

const int NUM_DMX_SLOTS = 12;

typedef uint8_t lightarr_t[NUM_DMX_SLOTS];

extern const lightarr_t
resetlightOffArray,
resetlightOnArray,
gamestartArray,
riddle1Array,
riddle2Array,
riddle3_1Array,
riddle3_2Array,
riddle4Array,
riddle6Array,
riddle7Array,
riddle8Array,
gamewon1Array,
gamewon2Array;

#endif /* LIGHT_H_ */
