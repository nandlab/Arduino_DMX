#ifndef CONFIG_H_
#define CONFIG_H_

#include <WiFi.h>

extern const unsigned esp32;
extern const char ssid[];
extern const char password[];

extern const IPAddress recipientIP;  // IP der Empfänger
extern const unsigned localPort;     // Port auf dem der ESP32 hört
extern const unsigned qlabPort;      // Port auf den der ESP32 die UDP schickt, in dem Fall Qlab
extern const unsigned supportPort;   // Port des Betreuers im PacketSender
extern const unsigned espPort;       // Port des ESPs im PacketSender

#endif /* CONFIG_H_ */
